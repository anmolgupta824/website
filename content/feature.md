---
title: Feature
# sidebar: true
# sidebarlogo: fresh-white-alt
include_footer: true

feature_title: Our Features
features:
    - title: Air Freight Pricing Index (AFPI) 
      text: Uses AI to present air freight rates, prices and trends.
      # Icon (from /images/illustrations/icons/___.svg)
      icon: AirFreight
    - title: Messaging 
      text: Our unique peer to peer messaging platform ensures your next business transaction is one text or phone call away.
      icon: messagingplatform
    - title: Quality Tracking
      text: We use feedback & active learning to determine the quality of potential clients. 
      icon: QualityTracking
    - title: Independent Platform
      text: We do not comply with airline shareholders or have preferred partners. Rest assured knowing that your data is secure.
      icon: IndependentPlatform 

---
---
title: Team
# sidebar: true
# sidebarlogo: fresh-white-alt
include_footer: true

team_title: Meet the team
team_subtitle: For Airlines

team:
    - Name: Matt Petot
      Position: CEO
      icon: https://cargoai.co/wp-content/uploads/2020/04/Matt-profile.jpg
      About: Over 15 years of experience in Logistics, Airfreight and Innovation.
      Linkedin: https://www.linkedin.com/in/anmol-gupta-21875a89/
      Email: 
      # Icon (from /images/illustrations/icons/___.svg)
    - Name: FX Gsell
      Position: CTO
      icon: https://cargoai.co/wp-content/uploads/2020/06/photo-FX.jpg
      About: Passionate Solutions Architect with a demonstrated history of working across various industries.
      Linkedin: 
      Email: 
      # Icon (from /images/illustrations/icons/___.svg)
    - Name: Cho Oo Wai
      Position: FULL STACK DEVELOPER
      icon: https://cargoai.co/wp-content/uploads/2020/04/Cho.jpg
      About: 3 years experienced Web developer.
      Linkedin: 
      Email: 
    - Name: Dipty Ojha
      Position: FULL STACK DEVELOPER
      icon: https://cargoai.co/wp-content/uploads/2020/05/Photo-Dipty.jpg
      About: Experienced and well rounded developper, always eager to learn and find new challenges.
      Linkedin: 
      Email: 
    - Name: Adrian Loh
      Position: BUSINESS ANALYST
      icon: https://cargoai.co/wp-content/uploads/2020/04/Adrian.jpg
      About: An inquisitive, multilingual data analyst that seeks to unravel data mysteries and provide meaningful insights. 
      Linkedin: 
      Email: 
    - Name: Hazwani Lee
      Position: HEAD OF BUSINESS DEVELOPMENT
      icon: https://cargoai.co/wp-content/uploads/2020/05/Hazwani.jpeg
      About: Highly experienced in all aspects of business and operations management in the airfreight Industry.
      Linkedin: 
      Email: 
    - Name: Jasmine Ng
      Position: FRONT END DEVELOPER
      icon: https://cargoai.co/wp-content/uploads/2020/05/jasmine_photo-1.jpg
      About: Technocrat. I’m self-taught, a creative problem solver and willing to dive into any challenge.
      Linkedin: 
      Email: 
    - Name: Arnaud Thiebault
      Position: FRONT END GURU
      icon: https://cargoai.co/wp-content/uploads/2020/05/photo-Arnaud.jpg
      About: Enthusiastic developer & always ready to bring his extensive knowledge from other industries.
      Linkedin: 
      Email: 
    - Name: Arthur St-Genis
      Position: FULL STACK DEVELOPER
      icon: https://cargoai.co/wp-content/uploads/2020/04/Arthur.jpg
      About: Passionate about building modern applications that allows me to continuously learn and discover new technology.
      Linkedin: 
      Email: 
    - Name: Damien Roustan
      Position: FULL STACK DEVELOPER
      icon: https://cargoai.co/wp-content/uploads/2020/05/Photo-Damien-150x150.jpg
      About: Innovative developer with experience on airfreight systems.
      Linkedin: 
      Email: 
    - Name: Anmol Gupta
      Position: PRODUCT MANAGER
      icon: https://cargoai.co/wp-content/uploads/2020/05/Photo-Anmol.jpg
      About: Great product building experience and entrepreneurial skills. 
      Linkedin: 
      Email: 
    - Name: Tay Liu Zhi
      Position: FULL STACK DEVELOPER
      icon: https://cargoai.co/wp-content/uploads/2020/06/LiuZhi.jpg
      About: Graduated BSC Mathematical Sciences from NTU majoring in Statistics.
      Linkedin: 
      Email:         
---
---
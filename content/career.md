---
title: Career
# sidebar: true
# sidebarlogo: fresh-white-alt
include_footer: true


jobs:
    - title: Full Stack Developer
      text: 
            - We are looking for full stack developers capable of producing scalable software solutions in the logistics marketplace space. You will be part of a cross-functional team responsible for the full software development life cycle – from conception to development. 

           
            - As a full stack developer, you should be comfortable with both front-end and back-end coding languages, development frameworks, and third-party libraries.

            - Here is list of examples of features on our current roadmap that you might work on 

      feature: 
              - Data Analytics Dashboards
              - Create mobile app from our web platform
              - AI/machine learning model to predict booking answer
              - API and integrations projects with airlines systems
              - Yield management and Pricing tool for Airline
              - Web scrapping and parsing bot
              - Ongoing optimization and improvement

      responsiblties:
                    - Work with the development teams closely to create software solutions
                    - Design client-side and server-side services and modules
                    - Develop and manage a well-functioning application
                    - Write effective and secure APIs
                    - Test software to ensure responsiveness, efficiency and security
      requirements:
                    - Between 0 – 5 years of professional experience in software development (logistics background is a plus)
                    - Education in computer science or similar qualifications
                    - Expert in back-end and front-end languages such as Java, Angular, Python, PHP, Go
                    - Experience working with desktop and/or mobile applications
                    - Familiarity with server management, databases, cloud services (preferably AWS)
                    - An analytical mind, organizational skills & excellent communication
                    - Great attention to detail
      compensation: 
                    - Our compensation package (to be negotiated individually) will commensurate with your experience     
           
          
      # Icon (from /images/illustrations/icons/___.svg)
      
      icon: laptop-globe
      index: 0
    - title: Front End Developer  
      text: 
          - We are looking for front-end developers capable of producing scalable software solutions in the logistics marketplace space. You will be part of a cross-functional team responsible for the full software development life cycle – from conception to development.
          - As a front-end developer, you should be comfortable with front-end coding languages, development frameworks, and third-party libraries.
          - Here is list of examples of features on our current roadmap that you might work on
      feature: 
              - Data Analytics Dashboards
              - Create mobile app from our web platform
              - Ongoing optimization and improvement
      responsiblties:
                    - Work with the development teams closely to create software solutions
                    - Design client-side and server-side frameworks
                    - Develop and manage a well-functioning application
                    - Test software to ensure responsiveness, efficiency and security
      requirements:
                    - Between 0-5 years of experience working with desktop and/or mobile applications
                    - Knowledge of multiple front-end languages 
                    - Familiarity with web-servers, databases, and UI/UX design
                    - An analytical mind, organizational skills & excellent communication
                    - Great attention to detail
      compensation: 
                    - Our compensation package (to be negotiated individually) will commensurate with your experience          

      icon: doc-sync
    - title: Customer Success
      text: 
          - We are looking for Customer Succes teamplayers capable of quickly help change the airfreight industry. You will be part of a cross-functional team responsible finding, onboarding, helping customers.
      responsiblties:
                    - Advise customer on inquiries, complaints and comments made on CargoAI support chat
                    - Providing a service that is highlighted by the warmth of treatment, customer care and fast and effective solutions to emerging situations
                    - To know in detail our Airline and Forwarder products to support customers on specific queries
                    - Partner with a number of CargoAI’s customers to grow their product usage
                    - Conduct product demos both in person and online
                    - Ensure and increase product adoption across the customers internal teams
                    - Help an entire industry changing outdated processes for good
                    - Develop and execute a playbook of initiatives and share them with the team
      requirements:
                    - You are great at working with and understanding people, and possess strong communication skills
                    - You like working with targets and are goal oriented 
                    - You are curious and driven to help our customers succeed
                    - You are able to set priorities right and have strong organizational skills
                    - You take the initiative and thrive working autonomously, with minimal supervision
                    - You want to grow personally and make an impact with your work
      compensation: 
                    - Our compensation package (to be negotiated individually) will commensurate with your experience  

      icon: mobile-feed
      index: 1
    - title: Digital Marketing
      text: 
          - You will be working directly with one of the co-founders and will be in charge of designing and executing a growth/marketing strategy 
      responsiblties:
                    - Create digital content (video, post, interviews) to post on the company website and linkedin
                    - Push business decisions to support the growth metrics.
                    - Monitor a diverse set of campaigns simultaneously for maximum engagement and exposure.
                    - Analyze keywords, web traffic, and market trends to make data based decisions.
                    - Produce reports on marketing campaigns and traffic growth periodically.
                    - Build an extensive network with bloggers, journalists, industry influencers.
                    - Keep updated with the latest best practices in the online marketing industry.
      requirements:
                    - Outside the box thinker and quick learner
                    - Analytical and innovative 
                    - Strong communication and problem solving skills
                    - Positive attitude and entrepreneurial spirit
                    - Willingness to get hands dirty in multiple arenas
                    - Enthusiasm for marketing and growth hacks
                    - Autonomy, sense of initiative and responsibility
                    - Perseverance and motivation
                    - You possess an excellent ability to adapt to new technologies in a dynamic and growing environment.
                    - Finally you want to discover the world of startups and have a key role
      compensation: 
                    - Our compensation package (to be negotiated individually) will commensurate with your experience      
      icon: mobile-feed  
      index: 2

---
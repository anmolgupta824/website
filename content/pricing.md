---
title: Pricing
# sidebar: true
# sidebarlogo: fresh-white-alt
include_footer: true


pricing_airline_title: Our Pricing
pricing_airline_subtitle: For Airlines
pricing_airline_subtitle2: For Forwarders

pricing_airline_features:
    - title: Basic
      text: Uses artificial intelligence to present air freight rates, prices and trends, and daily updates to customers that are subscribed.
      # Icon (from /images/illustrations/icons/___.svg)
      icon: Basic
    - title: Pro
      text: Our unique peer to peer messaging platform ensures your next business transaction is one text or phone call away.
      icon: pro
    - title: Enterprise
      text: We use feedback & active learning to determine the quality of potential clients. Thus, you can be confident that transactions will run smoothly and be on time.
      icon: Enterprise

pricing_forwarder_features:
    - title: Basic
      text: Uses artificial intelligence to present air freight rates, prices and trends, and daily updates to customers that are subscribed.
      # Icon (from /images/illustrations/icons/___.svg)
      icon: laptop-globe
    - title: Pro
      text: Our unique peer to peer messaging platform ensures your next business transaction is one text or phone call away.
      icon: doc-sync   

---
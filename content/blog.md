---
title: Blog
# sidebar: true
# sidebarlogo: fresh-white-alt
include_footer: true

featured_blog:
    - title: Aircargo Digitalization 
      text: Air freight is a key enabler of cross border trade which becomes critical for express deliveries, long-distance and efficient transportation of sensitive and critical goods. 
      icon: https://media-exp1.licdn.com/dms/image/C5612AQEijCIuG9hsZA/article-cover_image-shrink_720_1280/0?e=1600300800&v=beta&t=BeKrSCqQ6Nr0sALFrjwuDaBaIoR87E0OO1D7aWw4kfY
      article_link: https://www.linkedin.com/pulse/aircargo-digitalization-hazwani-lee/


primary_blog:
    - title: CargoAI- We bring AirCargo Digitization to the next level  
      text: CargoAI is the first and only airfreight booking platform which integrates data and Artificial Intelligence Technology providing seamless solution to both forwarders and airlines
      icon: https://media-exp1.licdn.com/dms/image/C5612AQEijCIuG9hsZA/article-cover_image-shrink_720_1280/0?e=1600300800&v=beta&t=BeKrSCqQ6Nr0sALFrjwuDaBaIoR87E0OO1D7aWw4kfY
      article_link: https://www.linkedin.com/pulse/cargoai-we-bring-aircargo-digitization-next-level-hazwani-lee/
    - title: Customer  & Opportunity Management  
      text: CargoAI is the first and only airfreight booking platform which integrates data and Artificial Intelligence Technology providing seamless solution to both forwarders and airlines
      icon: https://media-exp1.licdn.com/dms/image/C5612AQEijCIuG9hsZA/article-cover_image-shrink_720_1280/0?e=1600300800&v=beta&t=BeKrSCqQ6Nr0sALFrjwuDaBaIoR87E0OO1D7aWw4kfY
      article_link: https://www.linkedin.com/pulse/cargoai-we-bring-aircargo-digitization-next-level-hazwani-lee/

secondary_blog:
    - title: Customer  & Opportunity Management  
      text: Customer Reports 
      icon: https://media-exp1.licdn.com/dms/image/C5612AQEijCIuG9hsZA/article-cover_image-shrink_720_1280/0?e=1600300800&v=beta&t=BeKrSCqQ6Nr0sALFrjwuDaBaIoR87E0OO1D7aWw4kfY
      article_link: https://www.linkedin.com/pulse/aircargo-digitalization-hazwani-lee/
    - title: Customer  & Opportunity Management  
      text: Customer Reports 
      icon: https://media-exp1.licdn.com/dms/image/C5612AQEijCIuG9hsZA/article-cover_image-shrink_720_1280/0?e=1600300800&v=beta&t=BeKrSCqQ6Nr0sALFrjwuDaBaIoR87E0OO1D7aWw4kfY
      article_link: https://www.linkedin.com/pulse/aircargo-digitalization-hazwani-lee/
    - title: Customer  & Opportunity Management  
      text: Customer Reports 
      icon: https://media-exp1.licdn.com/dms/image/C5612AQEijCIuG9hsZA/article-cover_image-shrink_720_1280/0?e=1600300800&v=beta&t=BeKrSCqQ6Nr0sALFrjwuDaBaIoR87E0OO1D7aWw4kfY
      article_link: https://www.linkedin.com/pulse/aircargo-digitalization-hazwani-lee/
    - title: Customer  & Opportunity Management  
      text: Customer Reports 
      icon: https://media-exp1.licdn.com/dms/image/C5612AQEijCIuG9hsZA/article-cover_image-shrink_720_1280/0?e=1600300800&v=beta&t=BeKrSCqQ6Nr0sALFrjwuDaBaIoR87E0OO1D7aWw4kfY
      article_link: https://www.linkedin.com/pulse/aircargo-digitalization-hazwani-lee/ 

---